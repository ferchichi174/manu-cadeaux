	</div><!-- #main -->
		<?php 
			global $nuna_page_id;
			$nuna_settings = nuna_global_settings();
			$landing_style = 'footer-9';
			$footer_style = nuna_get_config('footer_style','');
			$footer_style = (get_post_meta( $nuna_page_id,'page_footer_style', true )) ? get_post_meta( $nuna_page_id, 'page_footer_style', true ) : $footer_style ;
			$header_style = nuna_get_config('header_style', ''); 
			$header_style  = (get_post_meta( $nuna_page_id, 'page_header_style', true )) ? get_post_meta($nuna_page_id, 'page_header_style', true ) : $header_style ;
		?>
		<?php if($footer_style && (get_post($footer_style)) && in_array( 'elementor/elementor.php', apply_filters('active_plugins', get_option( 'active_plugins' )))){ ?>
			<?php $elementor_instance = Elementor\Plugin::instance(); ?>
			<footer id="bwp-footer" class="bwp-footer <?php echo esc_attr( get_post($footer_style)->post_name ); ?>">
				<?php echo nuna_render_footer($footer_style);	?>
			</footer>
		<?php }else{
			nuna_copyright();
		}?>
	</div><!-- #page -->
	<div class="search-overlay">
		<div class="close-search"></div>
		<div class="wrapper-search">
			<?php nuna_search_form_product(); ?>		
		</div>	
	</div>
	<div class="bwp-quick-view">
	</div>	
	<?php 
		$back_active = nuna_get_config('back_active');
		if($back_active && $back_active == 1):
	?>
	<div class="back-top">
		<i class="arrow_carrot-up"></i>
	</div>
	<?php endif;?>
	<?php if((isset($nuna_settings['show-newletter']) && $nuna_settings['show-newletter']) && is_active_sidebar('newletter-popup-form') && function_exists('nuna_popup_newsletter')) : ?>		
		<?php nuna_popup_newsletter(); ?>
	<?php endif;  ?>
	<?php wp_footer(); ?>
	<div class="content-cart-popup">
	</div>
</body>
</html>