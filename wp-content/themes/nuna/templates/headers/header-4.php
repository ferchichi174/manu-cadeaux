	<?php 
		$nuna_settings = nuna_global_settings();
		$cart_layout = nuna_get_config('cart-layout','dropdown');
		$cart_style = nuna_get_config('cart-style','light');
		$show_minicart = (isset($nuna_settings['show-minicart']) && $nuna_settings['show-minicart']) ? ($nuna_settings['show-minicart']) : false;
		$show_compare = (isset($nuna_settings['show-compare']) && $nuna_settings['show-compare']) ? ($nuna_settings['show-compare']) : false;
		$enable_sticky_header = ( isset($nuna_settings['enable-sticky-header']) && $nuna_settings['enable-sticky-header'] ) ? ($nuna_settings['enable-sticky-header']) : false;
		$show_searchform = (isset($nuna_settings['show-searchform']) && $nuna_settings['show-searchform']) ? ($nuna_settings['show-searchform']) : false;
		$show_wishlist = (isset($nuna_settings['show-wishlist']) && $nuna_settings['show-wishlist']) ? ($nuna_settings['show-wishlist']) : false;
		$show_currency = (isset($nuna_settings['show-currency']) && $nuna_settings['show-currency']) ? ($nuna_settings['show-currency']) : false;
		$show_menutop = (isset($nuna_settings['show-menutop']) && $nuna_settings['show-menutop']) ? ($nuna_settings['show-menutop']) : false;
		$sticky_header = (isset($nuna_settings['enable-sticky-header']) && $nuna_settings['enable-sticky-header']) ? ($nuna_settings['enable-sticky-header']) : false;
		$background_header = get_post_meta( get_the_ID(), 'background_header', true );
	?>
	<h1 class="bwp-title hide"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
	<header id='bwp-header' class="bwp-header header-v4">
		<?php if($sticky_header) { nuna_menu_stcky(); } ?>
		<?php nuna_campbar(); ?>
		<?php nuna_menu_mobile(); ?>
		<div class="header-desktop">
			<?php if(($show_minicart || $show_wishlist || $show_searchform || is_active_sidebar('top-link')) && class_exists( 'WooCommerce' ) ){ ?>
			<div class="header-top">
				<div class="container">
					<div class="row">
						<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 header-left">
							<?php nuna_header_logo(); ?>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 header-center">
							<div class="header-search-form">
								<!-- Begin Search -->
								<?php if($show_searchform && class_exists( 'WooCommerce' )){ ?>
									<?php get_template_part( 'search-form' ); ?>
								<?php } ?>
								<!-- End Search -->	
							</div>
						</div>
						<div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12 header-right">
							<div class="header-page-link">
								<div class="login-header">
									<?php if (is_user_logged_in()) { ?>
										<?php if(is_active_sidebar('top-link')){ ?>
											<div class="block-top-link">
												<?php dynamic_sidebar( 'top-link' ); ?>
											</div>
										<?php } ?>
									<?php }else{ ?>
										<a class="active-login" href="#" >
										</a>
										<?php nuna_login_form(); ?>
									<?php } ?>
								</div>
								<?php if($show_wishlist && class_exists( 'WPCleverWoosw' )){ ?>
								<div class="wishlist-box">
									<a href="<?php echo WPcleverWoosw::get_url(); ?>"><i class="icon-heart2"></i></a>
									<span class="count-wishlist"><?php echo WPcleverWoosw::get_count(); ?></span>
								</div>
								<?php } ?>
								<?php if($show_minicart && class_exists( 'WooCommerce' )){ ?>
								<div class="nuna-topcart <?php echo esc_attr($cart_layout); ?> <?php echo esc_attr($cart_style); ?>">
									<?php get_template_part( 'woocommerce/minicart-ajax' ); ?>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class='header-wrapper' data-sticky_header="<?php echo esc_attr($nuna_settings['enable-sticky-header']); ?>">
				<div class="container">
					<div class="row">
						<div class="col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12 header-left">
							<div class="wpbingo-menu-mobile header-menu">
								<div class="header-menu-bg">
									<?php nuna_top_menu(); ?>
								</div>
							</div>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 header-right">
							<?php if( isset($nuna_settings['delivery']) && $nuna_settings['delivery'] ) : ?>
							<div class="delivery hidden-xs hidden-sm">
								<div class="content">
									<?php echo esc_html($nuna_settings['delivery']); ?>
								</div>
							</div>
							<?php endif; ?>
							<?php if( isset($nuna_settings['order']) && $nuna_settings['order'] ) : ?>
							<div class="order hidden-xs hidden-sm">
								<div class="content">
									<?php echo esc_html($nuna_settings['order']); ?>
								</div>
							</div>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div><!-- End header-wrapper -->
			<?php }else{ ?>
				<div class="header-normal">
					<div class='header-wrapper' data-sticky_header="<?php echo esc_attr($nuna_settings['enable-sticky-header']); ?>">
						<div class="container">
							<div class="row">
								<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 header-left">
									<?php nuna_header_logo(); ?>
								</div>
								<div class="col-xl-9 col-lg-9 col-md-6 col-sm-6 col-6 header-main">
									<div class="header-menu-bg">
										<?php nuna_top_menu(); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</header><!-- End #bwp-header -->