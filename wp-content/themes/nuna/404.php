<?php 
	get_header(); 
	$nuna_settings = nuna_global_settings();
?>
<div class="page-404">
	<div class="content-page-404">
		<div class="title-error">
			<?php if(isset($nuna_settings["title-error"]) && $nuna_settings["title-error"]){
				echo esc_html($nuna_settings["title-error"]);
			}else{
				echo esc_html__("404", "nuna");
			}?>
		</div>
		<div class="sub-title">
			<?php if(isset($nuna_settings["sub-title"]) && $nuna_settings["sub-title"]){
				echo esc_html($nuna_settings["sub-title"]);
			}else{
				echo esc_html__("Oops! That page can't be found.", "nuna");
			}?>
		</div>
		<div class="sub-error">
			<?php if(isset($nuna_settings["sub-error"]) && $nuna_settings["sub-error"]){
				echo esc_html($nuna_settings["sub-error"]);
			}else{
				echo esc_html__("We're really sorry but we can't seem to find the page you were looking for.", "nuna");
			}?>
		</div>
		<a class="btn" href="<?php echo esc_url( home_url('/') ); ?>">
			<?php if(isset($nuna_settings["btn-error"]) && $nuna_settings["btn-error"]){
				echo esc_html($nuna_settings["btn-error"]);}
			else{
				echo esc_html__("Back The Homepage", "nuna");
			}?>
		</a>
	</div>
</div>
<?php
get_footer();