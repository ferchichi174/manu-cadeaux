<?php
/***** Active Plugin ********/
add_action( 'tgmpa_register', 'nuna_register_required_plugins' );
function nuna_register_required_plugins() {
    $plugins = array(
		array(
            'name'               => esc_html__('Woocommerce', 'nuna'), 
            'slug'               => 'woocommerce', 
            'required'           => true
        ),
		array(
            'name'      		 => esc_html__('Elementor', 'nuna'),
            'slug'     			 => 'elementor',
            'required' 			 => true
        ),
        array(
            'name'               => esc_html__('Revolution Slider', 'nuna'),
            'slug'               => 'revslider',
            'source'             => get_template_directory() . '/plugins/revslider.zip', 
            'required'           => false,
        ),        
		array(
            'name'               => esc_html__('Wpbingo Core', 'nuna'), 
            'slug'               => 'wpbingo', 
            'source'             => get_template_directory() . '/plugins/wpbingo.zip',
            'required'           => true, 
        ),			
		array(
            'name'               => esc_html__('Redux Framework', 'nuna'), 
            'slug'               => 'redux-framework', 
            'required'           => true
        ),			
		array(
            'name'      		 => esc_html__('Contact Form 7', 'nuna'),
            'slug'     			 => 'contact-form-7',
            'required' 			 => false
        ),	
		array(
            'name'     			 => esc_html__('WPC Smart Wishlist for WooCommerce', 'nuna'),
            'slug'      		 => 'woo-smart-wishlist',
            'required' 			 => false
        ),		
		array(
            'name'     			 => esc_html__('WooCommerce Variation Swatches', 'nuna'),
            'slug'      		 => 'variation-swatches-for-woocommerce',
            'required' 			 => false
        ),
    );
    $config = array();
    tgmpa( $plugins, $config );
}