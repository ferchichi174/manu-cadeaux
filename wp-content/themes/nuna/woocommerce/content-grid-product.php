<?php
	global $product, $woocommerce_loop, $post;
	$nuna_settings = nuna_global_settings();
	if(!isset($layout_shop)){
		$layout_shop = nuna_get_config('layout_shop','1');
	}
	$stock = ( $product->is_in_stock() )? 'in-stock' : 'out-stock' ;
?>
<?php if ($layout_shop == '1') { ?>
	<?php
		remove_action('woocommerce_after_shop_loop_item', 'nuna_add_loop_wishlist_link', 18);
		remove_action('woocommerce_after_shop_loop_item', 'nuna_woocommerce_template_loop_add_to_cart', 15 );
	?>
<div class="products-entry content-product1 clearfix product-wapper">
	<div class="products-thumb">
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<div class='product-button'>
			<?php do_action('woocommerce_after_shop_loop_item'); ?>
		</div>
		<?php if($stock == "out-stock"): ?>
			<div class="product-stock">    
				<span class="stock"><?php echo esc_html__( 'Out Of Stock', 'nuna' ); ?></span>
			</div>
		<?php endif; ?>
	</div>
	<div class="products-content">
		<div class="contents">
			<?php 
				if(isset($nuna_settings['product-wishlist']) && $nuna_settings['product-wishlist'] && class_exists( 'WPCleverWoosw' ) ){
					nuna_add_loop_wishlist_link();
				}
			?>
			<h3 class="product-title"><a href="<?php esc_url(the_permalink()); ?>"><?php esc_html(the_title()); ?></a></h3>
			<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
			<div class="btn-atc">
				<?php nuna_woocommerce_template_loop_add_to_cart(); ?>
			</div>
		</div>
	</div>
</div>
<?php }elseif ($layout_shop == '2') { ?>
	<?php
		remove_action('woocommerce_after_shop_loop_item', 'nuna_woocommerce_template_loop_add_to_cart', 15 );
	?>
<div class="products-entry content-product2 clearfix product-wapper">
	<div class="products-thumb">
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<div class='product-button'>
			<?php do_action('woocommerce_after_shop_loop_item'); ?>
		</div>
		<?php if($stock == "out-stock"): ?>
			<div class="product-stock">    
				<span class="stock"><?php echo esc_html__( 'Out Of Stock', 'nuna' ); ?></span>
			</div>
		<?php endif; ?>
	</div>
	<div class="products-content">
		<div class="contents">
			<?php woocommerce_template_loop_rating(); ?>
			<h3 class="product-title"><a href="<?php esc_url(the_permalink()); ?>"><?php esc_html(the_title()); ?></a></h3>
			<?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?>
			<div class="btn-atc">
				<?php nuna_woocommerce_template_loop_add_to_cart(); ?>
			</div>
		</div>
	</div>
</div>
<?php } ?>