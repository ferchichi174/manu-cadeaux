<?php if($settings['list_tab']){ ?>
	<?php $j = 0; ?>
	<div class="bwp-slider-homepage <?php echo esc_attr($layout); ?>">
		<?php if($title1) { ?>
		<div class="content-title">
			<h2><?php echo wp_kses($title1,'social'); ?></h2>
		</div>
		<?php } ?>
		<div class="slick-carousel slick-carousel-center"  data-dots="<?php echo esc_attr($show_pag);?>"  data-nav="<?php echo esc_attr($show_nav);?>" data-columns4="<?php echo $columns4; ?>" data-columns3="<?php echo $columns3; ?>" data-columns2="<?php echo $columns2; ?>" data-columns1="<?php echo $columns1; ?>" data-columns1440="<?php echo $columns1440; ?>" data-columns="<?php echo $columns; ?>">
			<?php foreach ($settings['list_tab'] as  $item){ ?>
				<div class="item">
					<div class="item-content">
						<div class="item-info <?php echo esc_html($item['horizontal_align']); ?> <?php echo esc_html($item['vertical_align']); ?> <?php echo esc_html($item['text_align']); ?> <?php echo esc_html($item['animation_content']); ?>">
							<div class="content">
								<div class="content-sub">
									<?php if( $item['subtitle_slider'] && $item['subtitle_slider'] ){ ?>
										<div class="subtitle-slider"><?php echo wp_kses($item['subtitle_slider'],'social'); ?></div>
									<?php } ?>
									<?php if( $item['title_slider'] && $item['title_slider'] ){ ?>
										<h2 class="title-slider"><?php echo wp_kses($item['title_slider'],'social'); ?></h2>
									<?php } ?>
									<?php if( $item['description_slider'] && $item['description_slider' ]){ ?>
										<div class="description-slider"><?php echo wp_kses($item['description_slider'],'social'); ?></div>
									<?php } ?>
									<?php if( $item['button_slider'] && $item['button_slider'] ){ ?>
										<a class="button-slider" href="<?php echo wp_kses_post($item['buttonlink_slider']); ?>"><?php echo esc_html($item['button_slider']); ?></a>						
									<?php } ?>
								</div>
							</div>
						</div>
                        <div class="content-image">
							<?php if( $item['image'] && $item['image']['url'] ){ ?>
								<img src="<?php echo esc_url($item['image']['url']); ?>" alt="<?php echo esc_attr__('Image Slider','wpbingo'); ?>">
							<?php } ?>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
<?php }?>