<?php  
if( $category == '' ){
	return ;
}

$item_row = (isset($item_row) && $item_row) ? $item_row : 1;
$show_name = (isset($show_name) && $show_name) ? $show_name : 'true'; 
if( !is_array( $category ) ){
	$category = explode( ',', $category );
}
$widget_id = isset( $widget_id ) ? $widget_id : 'category_slide_'.rand().time(); 
?>
<div id="<?php echo $widget_id; ?>" class="bwp-woo-categories scroll-list <?php echo esc_attr($layout); ?>">
	<?php if( isset($subtitle) && $subtitle) : ?>
		<div class="bwp-categories-subtitle">					
			<?php echo ($subtitle); ?>							
		</div>	
	<?php endif;?>
	<?php if( $title1) : ?>
		<h3 class="bwp-categories-title"><?php echo esc_html( $title1 ); ?></h3>
	<?php endif; ?>
	<div class="content-scroll-list">
		<div class="content-list-categories">
			<div class="list-categories" data-columns4="<?php echo esc_attr($columns4); ?>" data-columns3="<?php echo esc_attr($columns3); ?>" data-columns2="<?php echo esc_attr($columns2); ?>" data-columns1="<?php echo esc_attr($columns1); ?>" data-columns="<?php echo esc_attr($columns); ?>">
					<div class="content-scoll grid">
					<?php
						foreach( $category as $j => $cat ){
							$term = get_term_by('slug', $cat, 'product_cat');
							if($term) :		
								$thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
								$thumbnail_id1 = get_term_meta( $term->term_id, 'thumbnail_id1', true );
								$icon_category = get_term_meta( $term->term_id, 'category_icon', true );
								$thumb = wp_get_attachment_url( $thumbnail_id );
								if(!$thumb)
									$thumb = wc_placeholder_img_src();
								
								$thumb1 = $thumbnail_id1;
								if(!$thumb1)
									$thumb1 = wc_placeholder_img_src();
								?>
								<div class="item-product-cat-content">
									<div class="content-image">
										<?php if(isset($show_thumbnail) && $show_thumbnail) : ?>
											<a href="<?php echo get_term_link( $term->term_id, 'product_cat' ); ?>">
											<div class="item-image">
												<?php if($thumb) : ?>
													<img src="<?php echo esc_url($thumb); ?>" alt="<?php echo $term->slug ;?>" />
												<?php endif ; ?>
											</div>
											</a>
										<?php endif;?>
										<?php if(isset($show_thumbnail1) && $show_thumbnail1) : ?>
											<div class="item-thumbnail">
												<img src="<?php echo esc_url($thumb1); ?>" alt="<?php echo $term->slug ;?>" />
											</div>
										<?php endif;?>							
										<?php if(isset($show_icon) && $show_icon) : ?>
											<?php if(isset($icon_category) && $icon_category) : ?>
												<div class="item-icon">
													<i class="<?php echo esc_attr($icon_category); ?>"></i>
												</div>
											<?php endif;?>
										<?php endif;?>
									</div>
									<div class="content">
										<?php if($show_name) : ?>
										<h2 class="item-title">
											<a href="<?php echo get_term_link( $term->term_id, 'product_cat' ); ?>"><?php echo esc_html( $term->name ); ?></a>
										</h2>
										<?php if( $label_button) : ?>
										<div class="btn-all">
											<a href="<?php echo esc_url($link_button); ?>"><?php echo esc_html($label_button) ?></a>
										</div>
										<?php endif;?>
										<?php endif;?>
										<?php if(isset($show_count) && $show_count) : ?>
										<div class="item-count">
											<?php if($term->count == 1){?>
												<?php echo esc_attr($term->count) .'<span>'. esc_html__(' products', 'wpbingo').'</span>'; ?>
											<?php }else{ ?>
												<?php echo esc_attr($term->count) .'<span>'. esc_html__(' products', 'wpbingo').'</span>'; ?>
											<?php } ?>
										</div>
										<?php endif;?>
									</div>
								</div>
							<?php endif; ?>		
					<?php } ?>
				</div>
			</div>
			<div class="scrollbar">
				<div class="handle">
				</div>
			</div>
			<?php if($show_nav) { ?>
				<div class="controls">
					<button class="btn prev"><i class="arrow_carrot-left"></i></button>
					<button class="btn next"><i class="arrow_carrot-right"></i></button>
				</div>
			<?php } ?>
		</div>
	</div>
</div>